var lat;
var lng;
console.log(window.location.search);

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
    	function(position) {
  			lat=position.coords.latitude;
  			console.log(lat);
  			lng=position.coords.longitude;
  			console.log(lng);
  			//&lat={float}&lng={float}
  			if (window.location.search==''){
  				window.location = "https://dev.travelingyuk.com/objek-wisata?lat="+lat+"&lng="+lng;
  			}
    	},
    	function showError(error) {
		    switch(error.code) {
		        case error.PERMISSION_DENIED:
		            console.log("User denied the request for Geolocation.");
		            break;
		        case error.POSITION_UNAVAILABLE:
		            console.log("Location information is unavailable.");
		            break;
		        case error.TIMEOUT:
		            console.log("The request to get user location timed out.");
		            break;
		        case error.UNKNOWN_ERROR:
		            console.log("An unknown error occurred.");
		            break;
		    }
		}
    );
} 
