var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var sassMiddleware = require('node-sass-middleware');
var myParser = require("body-parser");

var device = require('express-device');
var http = require("http");
var request = require('request');
//var baseUrl='https://dev.travelingyuk.com/';
var baseUrl='http://35.198.215.181:8000/';

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var searchsRouter = require('./routes/search');
var lokasisRouter = require('./routes/lokasi');
var kulinersRouter = require('./routes/kuliner');
var itinerarisRouter = require('./routes/itinerari');
var objeksRouter = require('./routes/objek');
var akomodasisRouter = require('./routes/akomodasi');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(device.capture());
app.use(myParser.json())


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));




app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/search', searchsRouter);
app.use('/lokasi', lokasisRouter);
app.use('/kuliner', kulinersRouter);
app.use('/akomodasi', akomodasisRouter);
app.use('/objek-wisata', objeksRouter);
app.use('/itinerari', itinerarisRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
