var express = require('express');
var router = express.Router();

var request = require('request');
var rp = require('request-promise');

var baseUrl='https://dev.travelingyuk.com/';
//var baseUrl='http://35.198.215.181:8000/';


/* GET home page. */
router.get('/', function(req, res, next) {
  	let keyword = req.query.keyword;    
	rp({url:'http://35.186.156.238/search?page=1&pageSize=4&q='+keyword, json:true})
    .then(function (rpSearch) {
    	var datas=rpSearch;
    	if(datas.advice.focus=='itineraries'){
	  		if(req.device.type=='phone') {	  			
			        res.render('itinerarisingle',{
			        	baseUrl:baseUrl,
				    	datas:datas,
			        	body:JSON.stringify(datas),
			        	keyword:keyword,
			        	menuaktif:"itinerari"
			        });		    	
			    } else {
		    		res.render('ditinerarisingle',{
			        	baseUrl:baseUrl,
				    	datas:datas,
			        	body:JSON.stringify(datas),
			        	keyword:keyword,
			        	menuaktif:"itinerari"
			        });		    	
			    }
	  	} else {
		  	rp({url:'https://admin.travelingyuk.com/wp-admin/admin-ajax.php?action=storisearch&s='+keyword, json:true})
    		.then(function (rpStori) {	
    			var storidatas= rpStori; 		  		
		  		if(req.device.type=='phone') {	  			
			        res.render('search',{
			        	baseUrl:baseUrl,
				    	datas:datas,
			        	body:JSON.stringify(datas),
			        	storidatas: storidatas,
			        	storibody: JSON.stringify(storidatas),
			        	keyword:keyword,
			        	menuaktif:"home"
			        });		    	
			    } else {
		    		res.render('dsearch',{
			        	baseUrl:baseUrl,
				    	datas:datas,
			        	body:JSON.stringify(datas),
			        	storidatas: storidatas,
			        	storibody: JSON.stringify(storidatas),
			        	keyword:keyword,
			        	menuaktif:"home"
			        });		    	
			    }
    		}).catch(function (err) {
		        console.log(err)
		    });
	    }
    }).catch(function (err) {
        console.log(err)
    });
});

module.exports = router;
