var express = require('express');
var router = express.Router();

var request = require('request');
var rp = require('request-promise');
var baseUrl='https://dev.travelingyuk.com/';
// var baseUrl='http://35.198.215.181:8000/';
	
/* GET home page. */
router.get('/', function(req, res, next) {
	rp({url:'http://35.186.156.238', json:true})
    .then(function (rpHome) {
    	var datas=rpHome;
    	rp({url:'https://admin.travelingyuk.com/wp-admin/admin-ajax.php?action=storihome', json:true})
	    .then(function (rpStoriHome) {
	    	var storidatas=rpStoriHome;
	    	if(req.device.type=='phone') {
		        res.render('index',{
		        	baseUrl:baseUrl,
		        	datas:datas,
			        storidatas: storidatas,
			        storibody:JSON.stringify(storidatas),
		        });
		    } else {
		    	res.render('dindex',{
		    		baseUrl:baseUrl,
		    		datas:datas,
			        storidatas: storidatas,
			        storibody:JSON.stringify(storidatas),
		        	menuaktif:"home",		     		
		    	});
		    }
	    	/*rp({url:'https://admin.travelingyuk.com/wp-json/wp/v2/posts?type=post&status=publish', json:true})
		    .then(function (rpFeature) {
		    	var featuredatas=rpFeature;		    			    	
		    }).catch(function (err) {
		        console.log(err)
		    });*/
	    }).catch(function (err) {
	        console.log(err)
	    });    		
    }).catch(function (err) {
        console.log(err)
    });	  	
});

router.get('/:slug/:id', function(req, res, next) {
	let id=req.params.id;
	
	rp({url:'https://travelingyuk.com/wp-json/wp/v2/posts/'+id, json:true})
    .then(function (rpStori) {
        var storidatas=rpStori;
        rp({url:'https://travelingyuk.com/wp-json/wp/v2/users/'+storidatas.author, json:true})
	    .then(function (rpAuthor) {
	        var authordatas=rpAuthor;
	        rp({url:'https://travelingyuk.com/wp-json/wp/v2/categories/'+storidatas.categories[storidatas.categories.length-1], json:true})
	        .then(function (rpCat) {
	        	var catdatas = rpCat;
				rp({url:'https://travelingyuk.com/wp-json/wp/v2/media/'+storidatas.featured_media, json:true})
			    .then(function (rpmedia) {
			    	var mediadatas=rpmedia;
			    	rp({url:'https://travelingyuk.com/wp-json/wp/v2/posts?filter[cat]='+storidatas.categories[storidatas.categories.length-1]+'&page=1&per_page=4&exclude='+storidatas.id, json:true})
				    .then(function (rpfeature) {
						var featuredatas=rpfeature;
						if(req.device.type=='phone') {
					        res.render('storisingle',{
					        	baseUrl:baseUrl,
						        storidatas: storidatas,
					        	storibody: JSON.stringify(storidatas),
					        	authordatas: authordatas,
					        	catdatas:catdatas,
					        	mediadatas:mediadatas,
					        	featuredatas:featuredatas,
					        	menuaktif:"stori",
					        	featurebody:JSON.stringify(featuredatas),
					        });
					    } else {
					    	res.render('dstorisingle',{
					    		baseUrl:baseUrl,
						        storidatas: storidatas,
					        	storibody: JSON.stringify(storidatas),
								menuaktif:"stori",
					        	authordatas: authordatas,
					        	catdatas:catdatas,
					        	mediadatas:mediadatas,
					        	featuredatas:featuredatas,
					        	featurebody:JSON.stringify(featuredatas),
					    	});
					    }
				    })
				    .catch(function (err) {
				        // API call failed...
				    });
			        
			    })
			    .catch(function (err) {
			        // API call failed...
			    });
		    }).catch(function (err) {
		        // API call failed...
		    });
	    }).catch(function (err) {
	        console.log(err)
	    });
    }).catch(function (err) {
        console.log(err)
    });
});
module.exports = router;
