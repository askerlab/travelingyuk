var express = require('express');
var router = express.Router();

var request = require('request');
var rp = require('request-promise');
var baseUrl='https://dev.travelingyuk.com/';
var apiUrl = "http://35.186.156.238";
//var baseUrl='http://35.198.215.181:8000/';


/* GET home page. */
router.get('/', function(req, res, next) {
  	var lat = req.query.lat;
	var lng = req.query.lng;
	console.log(lat);
	console.log(lng);
	if (lat){
		rp({url:'http://35.186.156.238/locations?lat='+lat+'&lng='+lng, json:true})
	    .then(function (rpLokasi) {
	    	var datas=rpLokasi;
	    	if(req.device.type=='phone') {
		        res.render('location',{
		        	baseUrl:baseUrl,
				    datas:datas,
					body:JSON.stringify(datas),
					menuaktif:"lokasi"
		        });
		    } else {
		    	res.render('dlocation',{
		    		baseUrl:baseUrl,
				    datas:datas,
					body:JSON.stringify(datas),
					menuaktif:"lokasi"
		    	});
		    }
	    }).catch(function (err) {
	        console.log(err)
	    });	
	} else {
		rp({url:'http://35.186.156.238/locations', json:true})
	    .then(function (rpLokasi) {
	    	var datas=rpLokasi;
	    	if(req.device.type=='phone') {
		        res.render('location',{
		        	baseUrl:baseUrl,
				    datas:datas,
					body:JSON.stringify(datas),
					menuaktif:"lokasi"
		        });
		    } else {
		    	res.render('dlocation',{
		    		baseUrl:baseUrl,
				    datas:datas,
					body:JSON.stringify(datas),
					menuaktif:"lokasi"
		    	});
		    }
	    }).catch(function (err) {
	        console.log(err)
	    });
	}
});

router.get('/view/:slug', function(req, res, next){
	let slug = req.params.slug;
	rp({
		url: apiUrl + "/locations/" + slug,
		json: true
	})
	.then(function (data){
		// Desktop
		if( req.device.type == 'phone') {
			res.send("Using phone template!");
		} else {
			res.render('dlocationdetail', {
				baseUrl:baseUrl,
			  datas:data,
				body:JSON.stringify(data),
				menuaktif:"lokasi",
			});
		}
	}).catch(function (err){
		console.log(err);
	});
});

router.get('/:slug/:id', function(req, res, next) {
	let id=req.params.id;
	let slug=req.params.slug;
	slug=slug.split('-').join(' ');
	rp({url:'http://35.186.156.238/locations/'+id, json:true})
    .then(function (rpLokasi) {
    	var datas = rpLokasi;
		rp({url:'https://admin.travelingyuk.com/wp-json/wp/v2/posts?type=post&status=publish&search='+slug, json:true})
    	.then(function (rpStori) {
    		var storidatas = rpLokasi;
    		if(req.device.type=='phone') {
		        res.render('locationsingle',{
		        	baseUrl:baseUrl,
				    datas:datas,
					body: JSON.stringify(datas),
					storidatas:storidatas,
					storibody:JSON.stringify(storidatas),
					menuaktif:"lokasi"
		        });
		    } else {
		    	res.render('dlocationsingle',{
		    		baseUrl:baseUrl,
				    datas:datas,
					body:JSON.stringify(datas),
					menuaktif:"lokasi",
					storidatas:storidatas,
					storibody:JSON.stringify(storidatas),
		    	});
	    	}
    	}).catch(function (err) {
	        console.log(err)
	    });
   	}).catch(function (err) {
        console.log(err)
    }); 
});	
module.exports = router;
