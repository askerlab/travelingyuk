var express = require('express');
var router = express.Router();

var request = require('request');
var rp = require('request-promise');
var baseUrl='https://dev.travelingyuk.com/';
var apiUrl = "http://35.186.156.238";
//var baseUrl='http://35.198.215.181:8000/';


/* GET home page. */
router.get('/', function(req, res, next) {
	var lat = req.query.lat;
	var lng = req.query.lng;
	console.log(lat);
	console.log(lng);
	if (lat){
		rp({url:'http://35.186.156.238/accommodations?lat='+lat+'&lng='+lng, json:true})
	    .then(function (rpAkomodasi) {
	    	var datas=rpAkomodasi;
	    	if(req.device.type=='phone') {
		        res.render('akomodasi',{
		        	baseUrl:baseUrl,
				    datas:datas,
					body:JSON.stringify(datas),
					ip:req.headers['x-real-ip'],
					menuaktif:"akomodasi"
		        });
		    } else {
		    	res.render('dakomodasi',{
		    		baseUrl:baseUrl,
				    datas:datas,
					body:JSON.stringify(datas),
					ip:req.headers['x-real-ip'],
					menuaktif:"akomodasi"
		    	});
		    }
	    }).catch(function (err) {
	        console.log(err)
	    });
	} else {		
		rp({url:'http://35.186.156.238/accommodations', json:true})
	    .then(function (rpAkomodasi) {
	    	var datas=rpAkomodasi;
	    	if(req.device.type=='phone') {
		        res.render('akomodasi',{
		        	baseUrl:baseUrl,
				    datas:datas,
					body:JSON.stringify(datas),
					ip:req.headers['x-real-ip'],
					menuaktif:"akomodasi"
		        });
		    } else {
		    	res.render('dakomodasi',{
		    		baseUrl:baseUrl,
				    datas:datas,
					body:JSON.stringify(datas),
					ip:req.headers['x-real-ip'],
					menuaktif:"akomodasi"
		    	});
		    }
	    }).catch(function (err) {
	        console.log(err)
	    });		
	}
});

router.get('/view/:slug', function(req, res, next){
	let slug = req.params.slug;
	rp({
		url: apiUrl + "/accommodations/" + slug,
		json: true
	})
	.then(function (data){
		// res.send(data);
		// Desktop
		res.render('dakomodasidetail', {
			baseUrl:baseUrl,
		  datas:data,
			body:JSON.stringify(data),
			menuaktif:"akomodasi",
		});
	}).catch(function (err){
		console.log(err);
	});
});

router.get('/:slug/:id', function(req, res, next) {
	let id=req.params.id;
	let slug=req.params.slug;
	slug=slug.split('-').join(' ');
	rp({url:'http://35.186.156.238/accommodations/'+id, json:true})
    .then(function (rpAkomodasi) {
    	var datas=rpAkomodasi;
    	rp({url:'https://admin.travelingyuk.com/wp-json/wp/v2/posts?type=post&status=publish&search='+slug, json:true})
	    .then(function (rpStori) {
	    	var storidatas=rpStori;
	    	if(req.device.type=='phone') {
		        res.render('akomodasisingle',{
		        	baseUrl:baseUrl,
				    datas:datas,
					body:JSON.stringify(datas),
					storidatas:storidatas,
					storibody:JSON.stringify(storidatas),
					ip:req.headers['x-real-ip'],
					menuaktif:"akomodasi"
		        });
		    } else {
		    	res.render('dakomodasisingle',{
		    		baseUrl:baseUrl,
				    datas:datas,
					body:JSON.stringify(datas),
					menuaktif:"akomodasi",
					ip:req.headers['x-real-ip'],
					storidatas:storidatas,
					storibody:JSON.stringify(storidatas),
		    	});
		    }
    	}).catch(function (err) {
	        console.log(err)
	    });
    }).catch(function (err) {
        console.log(err)
    });
});

module.exports = router;
