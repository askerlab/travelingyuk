var express = require('express');
var router = express.Router();

var request = require('request');
var rp = require('request-promise');
var baseUrl='https://dev.travelingyuk.com/';
var apiUrl = "http://35.186.156.238";
//var baseUrl='http://35.198.215.181:8000/';


/* GET home page. */
router.get('/', function(req, res, next) {
	var lat = req.query.lat;
	var lng = req.query.lng;
	console.log(lat);
	console.log(lng);
	if (lat){
		rp({url:'http://35.186.156.238/tourisms?lat='+lat+'&lng='+lng, json:true})
	    .then(function (rpTourism) {
	    	var datas=rpTourism;
	    	if(req.device.type=='phone') {
		        res.render('objek',{
		        	baseUrl:baseUrl,
				    datas:datas,
					body:JSON.stringify(datas),
					menuaktif:"objek"
		        });
		    } else {
		    	res.render('dobjek',{
		    		baseUrl:baseUrl,
		    		datas:datas,
					body:JSON.stringify(datas),
					menuaktif:"objek"
		    	});
		    }
	    }).catch(function (err) {
	        console.log(err)
	    });
	} else {
		rp({url:'http://35.186.156.238/tourisms', json:true})
	    .then(function (rpTourism) {
	    	var datas=rpTourism;
	    	if(req.device.type=='phone') {
		        res.render('objek',{
		        	baseUrl:baseUrl,
				    datas:datas,
					body:JSON.stringify(datas),
					menuaktif:"objek"
		        });
		    } else {
		    	res.render('dobjek',{
		    		baseUrl:baseUrl,
		    		datas:datas,
					body:JSON.stringify(datas),
					menuaktif:"objek"
		    	});
		    }
	    }).catch(function (err) {
	        console.log(err)
	    });
    }
});

router.get('/view/:slug', function(req, res, next){
	let slug = req.params.slug;
	rp({
		url: apiUrl + "/tourisms/" + slug,
		json: true
	})
	.then(function (data){
		// res.send(data);
		// Desktop
		res.render('dobjekdetail', {
			baseUrl:baseUrl,
		  datas:data,
			body:JSON.stringify(data),
			menuaktif:"objek",
		});
	}).catch(function (err){
		console.log(err);
	});
});

router.get('/:slug/:id', function(req, res, next) {
	let id=req.params.id;
	let slug=req.params.slug;
	slug=slug.split('-').join(' ');
	console.log(req.headers['x-real-ip']);
	rp({url:'http://35.186.156.238/tourisms/'+id+'?ip='+req.headers['x-real-ip'], json:true})
    .then(function (rpTourism) {
    	var datas=rpTourism;
    	rp({url:'https://admin.travelingyuk.com/wp-json/wp/v2/posts?type=post&status=publish&search='+slug, json:true})
	    .then(function (rpStori) {
	    	var storidatas=rpStori;
	    	if(req.device.type=='phone') {
		        res.render('objeksingle',{
		        	baseUrl:baseUrl,
				    datas:datas,
					body:JSON.stringify(datas),
					storidatas:storidatas,
					storibody:JSON.stringify(storidatas),
					ip:req.headers['x-real-ip'],
					menuaktif:"objek"
		        });
		    } else {
		    	res.render('dobjeksingle',{
		    		baseUrl:baseUrl,
				    datas:datas,
					body:JSON.stringify(datas),
					menuaktif:"objek",
					storidatas:storidatas,
					ip:req.headers['x-real-ip'],
					storibody:JSON.stringify(storidatas),
		    	});
		    }
    	}).catch(function (err) {
	        console.log(err)
	    });	
	}).catch(function (err) {
        console.log(err)
    });
});

module.exports = router;
