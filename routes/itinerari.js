var express = require('express');
var router = express.Router();

var request = require('request');

var baseUrl='https://dev.travelingyuk.com/';
//var baseUrl='http://35.198.215.181:8000/';


/* GET home page. */
router.get('/', function(req, res, next) {
  	request('http://35.186.156.238/itineraries', function (error, response, body) {
		var datas=JSON.parse(body); 	
	  	if(req.device.type=='phone') {
	        res.render('itinerari',{
	        	baseUrl:baseUrl,
			    datas:datas,
				body:body,
				menuaktif:"itinerari"
	        });
	    } else {
	    	res.render('ditinerari',{
	    		baseUrl:baseUrl,
			    datas:datas,
				body:body,
				menuaktif:"itinerari"
	    	});
	    }
	});
});

module.exports = router;
